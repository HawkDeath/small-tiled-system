#ifndef TILE_HPP
#define TILE_HPP

#include <cstdint>
#include <iostream>

#include <SFML/Graphics.hpp>

namespace urd
{

enum class TileType : std::int32_t
{
  None,
  Grass,
  Tree,
  Water,
  Lava,
  Unknown,
  Count
};

class Tile
{
    public:
    // explicit Tile() = default;
    explicit Tile(TileType type = TileType::None, std::uint32_t width = 32, std::uint32_t height = 32, sf::Texture *texture = nullptr) :
            mWidth(width), mHeight(height), mType(type), mTexture(texture)
    {
        mShape = sf::RectangleShape(sf::Vector2f(mWidth, mHeight));
        if (mTexture != nullptr)
            mShape.setTexture(mTexture);
    }
    ~Tile()
    {
        mTexture = nullptr;
    }

    void setTexture(sf::Texture *texture)
    {
        if (texture != nullptr)
        {
            mTexture = texture;

            mShape.setTexture(mTexture);
        }
        else
            std::cerr << "Texture is a null\n";
    }

    std::uint32_t getWidth() const {
        return mWidth;
    }

    std::uint32_t getHeight() const {
        return mHeight;
    }

    TileType getType() const {
        return mType;
    }

    void setPosition(const sf::Vector2f &pos)
    {
        mShape.setPosition(pos);
    }

    sf::Texture *getTexturePtr() const {
        return mTexture;
    }


    sf::RectangleShape& getShape() {
        return mShape;
    }

    private:
    std::uint32_t mWidth;
    std::uint32_t mHeight;
    TileType mType;

    sf::RectangleShape  mShape;
    sf::Texture *mTexture = nullptr;

};
}


#endif

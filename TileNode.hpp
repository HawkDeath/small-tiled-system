#ifndef __TILENODE_HPP__
#define __TILENODE_HPP__

#include <utility>

#include "Tile.hpp"

namespace urd
{

struct TileNode
{
    TileNode() = default;

    TileNode(TileNode &  other) = default;
    TileNode(TileNode && other) = default;

    TileNode & operator = (TileNode & other) = default;
    TileNode & operator = (TileNode && other) = default;

    virtual ~TileNode()
    {
        this->reset();
    }

    virtual void copy(TileNode & other)
    {
        this->left    = other.left;
        this->right   = other.right;
        this->current = other.current;
    }

    virtual void copy(TileNode && other)
    {
        this->copy(std::move(other));
    }

    virtual void reset()
    {
        this->left    = nullptr;
        this->right   = nullptr;
        this->current = nullptr;
    }

    Tile * left    = nullptr;
    Tile * right   = nullptr;
    Tile * current = nullptr;
};

}

#endif // __TILENODE_HPP__

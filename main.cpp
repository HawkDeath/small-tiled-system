#include <SFML/Graphics.hpp>
#include <vector>

#include "TilesContainer.hpp"
#include "TileCanvas.hpp"

constexpr std::uint32_t TILE_SIZE = 96;

int main()
{
    sf::RenderWindow window(sf::VideoMode(1280, 720), "Tiles system");

    sf::Image atlas;
    if(!atlas.loadFromFile("terrain.png"))
    {
        std::cerr << "failed load terrain.png\n";
        return -1;
    }

    sf::Texture grass;
    grass.loadFromImage(atlas, sf::IntRect(0, 256, 96, 96));

    sf::Texture water;
    water.loadFromImage(atlas, sf::IntRect(480, 448, 96, 96));

    sf::Texture lava;
    lava.loadFromImage(atlas, sf::IntRect(480, 64, 96, 96));


    urd::Tile tileGrass(urd::TileType::Grass, TILE_SIZE, TILE_SIZE, &grass);
    urd::Tile tileWater(urd::TileType::Water, TILE_SIZE, TILE_SIZE, &water);
    urd::Tile tileLava(urd::TileType::Lava, TILE_SIZE, TILE_SIZE, &lava);

    urd::TilesContainer::addTile(tileGrass, "grass");
    urd::TilesContainer::addTile(tileWater, "water");
    urd::TilesContainer::addTile(tileLava, "lava");

    std::vector<urd::TileCanvas*> canvans;

    urd::TileNode grassNode;
    grassNode.current = &urd::TilesContainer::getTile("grass");

    urd::TileCanvas grassCanvas(grassNode);
    grassCanvas.setPosition(sf::Vector2f(640, 350));
    canvans.push_back(&grassCanvas);

    urd::TileNode waterNode;
    waterNode.current = &urd::TilesContainer::getTile("water");

    urd::TileCanvas waterCanvas(waterNode);
    urd::TileCanvas waterCanvas2(waterNode);
    grassCanvas.setWest(&waterCanvas2);
    grassCanvas.setNorth(&waterCanvas);

    canvans.push_back(&waterCanvas);
    canvans.push_back(&waterCanvas2);

    urd::TileNode lavaNode;
    lavaNode.current = &urd::TilesContainer::getTile("lava");

    urd::TileCanvas lavaCanvas(lavaNode);
    grassCanvas.setSouth(&lavaCanvas);

    canvans.push_back(&lavaCanvas);


    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        for (auto &i : canvans)
            window.draw(*i);
        window.display();
    }

    return 0;
}


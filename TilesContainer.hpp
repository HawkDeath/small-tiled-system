#ifndef __TILESCONTAINER_HPP__
#define __TILESCONTAINER_HPP__

#include <string>
#include <map>

#include <Tile.hpp>

namespace urd
{
class TilesContainer
{
public:
    TilesContainer() = default;

    TilesContainer(TilesContainer &  other) = delete;
    TilesContainer(TilesContainer && other) = delete;

    TilesContainer & operator = (TilesContainer & other) = delete;
    TilesContainer & operator = (TilesContainer && other) = delete;

    virtual ~TilesContainer();

    static void addTile(Tile tile, std::string name)
    {
      //  mTiles.insert(std::make_pair(name, tile));
        mTiles[name] = tile;
    }

    static Tile& getTile(const std::string &name) {
        return mTiles.find(name)->second;
    }

private:
    static std::map<std::string, Tile> mTiles;

};

std::map<std::string, Tile> TilesContainer::mTiles;

}

#endif // __TILESCONTAINER_HPP__

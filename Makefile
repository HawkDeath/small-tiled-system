CC=g++ -std=c++17
CFLAGS=-c -Wall -fpermissive
INCLUDES := -I.
LDFLAGS=
LIBS=-lsfml-graphics -lsfml-window -lsfml-system
SOURCES=main.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=

ifeq ($(OS), Windows_NT)
    EXECUTABLE=tile-system.exe
else
    EXECUTABLE=tile-system
endif

.PHONY: all

all: $(SOURCES) $(EXECUTABLE)
	rm -rf *.o

$(EXECUTABLE): $(OBJECTS) 
	    $(CC) $(LDFLAGS) $(OBJECTS) $(LIBS) -o $@

.cpp.o:
	    $(CC) $(CFLAGS) $(INCLUDES) $(LIBS) $< -o $@

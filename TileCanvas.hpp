#ifndef TILECANVANS_HPP
#define TILECANVANS_HPP

#include <TileNode.hpp>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>

namespace urd
{

enum class TileDir : std::uint8_t
{
  North = 0,
  South = 1,
  West = 2,
  East = 3,
};


class TileCanvas : public sf::Drawable
{
public:
    explicit TileCanvas(TileNode node)
    {
        mNode = node;
    }

    void setPosition(const sf::Vector2f &position)
    {
        mPosition = position;
        mNode.current->setPosition(mPosition);
    }

    sf::Vector2f getPosition() const {
        return mPosition;
    }

    void setNorth(TileCanvas *north)
    {
        if (north != nullptr)
        {
            mNorth = north;
            Tile *tile = mNorth->getDefaultTile();
            mNorth->setPosition(sf::Vector2f(mPosition.x, mPosition.y - tile->getHeight()));
        }
    }

    void setSouth(TileCanvas *south)
    {
        if (south != nullptr)
        {
            mSouth = south;
            Tile *tile = mSouth->getDefaultTile();
            mSouth->setPosition(sf::Vector2f(mPosition.x, mPosition.y + tile->getHeight()));
        }
    }

    void setEast(TileCanvas *east)
    {
        if (east != nullptr)
        {
            mEast = east;
            Tile *tile = mEast->getDefaultTile();
            mEast->setPosition(sf::Vector2f(mPosition.x + tile->getWidth(), mPosition.y));
        }
    }

    void setWest(TileCanvas *west)
    {
        if (west != nullptr)
        {
            mWest = west;
            Tile *tile = mWest->getDefaultTile();
            mWest->setPosition(sf::Vector2f(mPosition.x - tile->getWidth(), mPosition.y));
        }
    }

    TileCanvas *getDir(const TileDir dir) const {
        switch(dir)
        {
        case TileDir::East:
            return mEast;
        case TileDir::West:
            return mWest;
        case TileDir::North:
            return mNorth;
        case TileDir::South:
            return mSouth;
        default:
            return nullptr;
        }
    }

    Tile *getDefaultTile() const {
        return mNode.current;
    }


private:
    void draw(sf::RenderTarget &rt, sf::RenderStates states) const override
    {
        auto shape = mNode.current->getShape();
        shape.setPosition(mPosition);
        rt.draw(shape, states);

        // debug print
        // std::cout << shape.getPosition().x << ", " << shape.getPosition().y << '\n';
    }

private:
    sf::Vector2f mPosition = sf::Vector2f(0.0f, 0.0f);

    TileNode mNode;
    TileCanvas *mNorth = nullptr;
    TileCanvas *mSouth = nullptr;
    TileCanvas *mWest = nullptr;
    TileCanvas *mEast = nullptr;

};

}

#endif
